HealthCardReader
=========
HealthCardReader is a Java library that can easily read German eGKs (elektronische Gesundsheitskarten).
The Java module SmartCardIO is used in the background.

Copyright (C) 2021 Johannes Pollitt
=========
All rights reserved
