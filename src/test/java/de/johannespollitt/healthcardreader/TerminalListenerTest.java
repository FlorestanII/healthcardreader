package de.johannespollitt.healthcardreader;

import de.johannespollitt.healthcardreader.command.CardReader;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TerminalListenerTest {

    @Test
    void testNull() {
        assertThrows(IllegalArgumentException.class, () -> new TerminalListener(null, null));
        assertThrows(IllegalArgumentException.class, () -> new TerminalListener(new HealthCardReader(), null));
        assertThrows(IllegalArgumentException.class, () -> new TerminalListener(null, mock(CardTerminal.class)));
    }

    @Test
    void testReadFailNullCallback() throws CardException {

        CardTerminal terminal = mock(CardTerminal.class);
        when(terminal.getName()).thenReturn("MyTerminalName");
        when(terminal.isCardPresent()).thenReturn(true);

        Card card = mock(Card.class);

        when(terminal.connect("*")).thenReturn(card);

        HealthCardReader reader = mock(HealthCardReader.class);

        when(reader.isRunning()).thenReturn(true).thenReturn(false);

        when(reader.getCardReadFailedCallback()).thenReturn(null);

        TerminalListener listener = new TerminalListener(reader, terminal);

        try (MockedConstruction<CardReader> mocked = mockConstruction(CardReader.class, (mock, context) -> {
            when(mock.read()).thenReturn(null);
        })) {
            listener.run();

            assertTrue(listener.isCardPresent());
            assertFalse(listener.isRunning());
        }

    }

    @Test
    void testReadWithCallback() throws CardException {

        CardTerminal terminal = mock(CardTerminal.class);
        when(terminal.getName()).thenReturn("MyTerminalName");
        when(terminal.isCardPresent()).thenReturn(true);

        Card card = mock(Card.class);

        when(terminal.connect("*")).thenReturn(card);

        HealthCardReader reader = mock(HealthCardReader.class);

        when(reader.isRunning()).thenReturn(true).thenReturn(false);
        
        Map<String, HealthCardData> readCards = new HashMap<>();
        
        when(reader.getCardInsertCallback()).thenReturn((t, c) -> {
        	readCards.put(t, c);
        });
        
        TerminalListener listener = new TerminalListener(reader, terminal);

        try (MockedConstruction<CardReader> mocked = mockConstruction(CardReader.class, (mock, context) -> {
        	HealthCardData data = new HealthCardData();
        	data.setVorname("MyVorname");
        	data.setNachname("MyNachname");
        	
            when(mock.read()).thenReturn(data);
        })) {
            listener.run();

            assertEquals(1, readCards.size());
            assertNotNull(readCards.get("MyTerminalName"));
            assertEquals("MyVorname", readCards.get("MyTerminalName").getVorname());
            assertEquals("MyNachname", readCards.get("MyTerminalName").getNachname());
            
            assertTrue(listener.isCardPresent());
            assertFalse(listener.isRunning());
        }

    }
    
    @Test
    void testReadFailWithCallback() throws CardException {

        CardTerminal terminal = mock(CardTerminal.class);
        when(terminal.getName()).thenReturn("MyTerminalName");
        when(terminal.isCardPresent()).thenReturn(true);

        Card card = mock(Card.class);

        when(terminal.connect("*")).thenReturn(card);

        HealthCardReader reader = mock(HealthCardReader.class);

        when(reader.isRunning()).thenReturn(true).thenReturn(false);

        AtomicBoolean calledCallback = new AtomicBoolean(false);

        when(reader.getCardReadFailedCallback()).thenReturn((terminalName) -> {
            calledCallback.set(true);
        });

        TerminalListener listener = new TerminalListener(reader, terminal);

        try (MockedConstruction<CardReader> mocked = mockConstruction(CardReader.class)) {
            listener.run();
        }

        assertTrue(listener.isCardPresent());
        assertTrue(calledCallback.get());
    }


}