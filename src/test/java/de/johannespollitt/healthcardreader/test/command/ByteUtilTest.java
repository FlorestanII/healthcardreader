package de.johannespollitt.healthcardreader.test.command;

import de.johannespollitt.healthcardreader.command.ByteUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ByteUtilTest {

    @Test
    public void testIntToByteArray() {
        Assertions.assertArrayEquals(new byte[]{0, 1, 2, 3, 4}, ByteUtil.intToByteArray(0, 1, 2, 3, 4));
    }

    @Test
    public void testUngzipFail() {
        String result = ByteUtil.ungzip(new byte[]{0, 0, 0});

        assertNull(result);
    }

    @Test
    public void testUngzip() {
        byte[] input = new byte[]{31, -117, 8, 0, 0, 0, 0, 0, 0, -1, 11, 73, -83, 40, 1, 0, -7, 8, -71, -101, 4, 0, 0, 0};

        String result = ByteUtil.ungzip(input);

        assertEquals("Text", result);
    }

}