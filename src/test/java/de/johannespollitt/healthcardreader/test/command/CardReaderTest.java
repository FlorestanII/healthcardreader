package de.johannespollitt.healthcardreader.test.command;

import de.johannespollitt.healthcardreader.HealthCardData;
import de.johannespollitt.healthcardreader.command.ByteUtil;
import de.johannespollitt.healthcardreader.command.CardReader;
import org.junit.jupiter.api.Test;

import javax.smartcardio.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.zip.GZIPOutputStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CardReaderTest {

    @Test
    public void testNullCard() {
        assertThrows(IllegalArgumentException.class, () -> new CardReader(null));
    }

    @Test
    public void testSelectMFFail() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00)));
            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            assertNull(reader.read());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testSelectHCAFail() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00)));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            assertNull(reader.read());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testZeroLengthEF_PD() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00, 0x90, 0x00)));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            assertNull(reader.read());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testXMLParseFail() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));
        CommandAPDU readLengthEF_VD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x82, 0x00, 0x08));

        byte[] data = compressGZIP("Text123");

        CommandAPDU readEF_PDFile = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02, data.length));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, data.length+2, 0x90, 0x00)));
            when(channel.transmit(readEF_PDFile)).thenReturn(new ResponseAPDU(combineArrays(data, ByteUtil.intToByteArray(0x90, 0x00))));
            when(channel.transmit(readLengthEF_VD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x00)));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            assertNull(reader.read());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testEmptyPDXML() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));
        CommandAPDU readLengthEF_VD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x82, 0x00, 0x08));

        byte[] data = compressGZIP("<root></root>");

        CommandAPDU readEF_PDFile = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02, data.length));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, data.length+2, 0x90, 0x00)));
            when(channel.transmit(readEF_PDFile)).thenReturn(new ResponseAPDU(combineArrays(data, ByteUtil.intToByteArray(0x90, 0x00))));
            when(channel.transmit(readLengthEF_VD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x00)));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            HealthCardData cardData = reader.read();
            assertNotNull(cardData);

            assertNull(cardData.getVersichertenID());
            assertNull(cardData.getVorname());
            assertNull(cardData.getNachname());
            assertNull(cardData.getGeburtsdatum());
            assertNull(cardData.getGeschlecht());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testPDandVDXML() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));
        CommandAPDU readLengthEF_VD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x82, 0x00, 0x08));

        byte[] dataPD = compressGZIP("<root>" +
                "<Versicherten_ID>A123456789</Versicherten_ID>" +
                "<Person>" +
                "<Geburtsdatum>20211020</Geburtsdatum>" +
                "<Vorname>MyVorname</Vorname>" +
                "<Nachname>MyNachname</Nachname>" +
                "<Geschlecht>D</Geschlecht>" +
                "<Vorsatzwort>MyVorsatzwort</Vorsatzwort>" +
                "<Namenszusatz>MyNamenszusatz</Namenszusatz>" +
                "<Titel>MyTitel</Titel>" +
                "<PostfachAdresse>" +
                "<Postleitzahl>12345</Postleitzahl>" +
                "<Ort>MyOrt</Ort>" +
                "<Postfach>MyPostfach</Postfach>" +
                "<Wohnsitzlaendercode>D</Wohnsitzlaendercode>" +
                "</PostfachAdresse>" +
                "<StrassenAdresse>" +
                "<Postleitzahl>12345</Postleitzahl>" +
                "<Ort>MyOrt</Ort>" +
                "<Strasse>MyStrasse</Strasse>" +
                "<Hausnummer>1A</Hausnummer>" +
                "<Anschriftenzusatz>MyZusatz</Anschriftenzusatz>" +
                "<Wohnsitzlaendercode>D</Wohnsitzlaendercode>" +
                "</StrassenAdresse>" +
                "</Person>" +
                "</root>");
        byte[] dataVD = compressGZIP("<root>" +
                "</root>");

        System.out.println(dataPD.length);
        System.out.println(dataVD.length);

        int maxRead = 0xFC;

        CommandAPDU readEF_PDFilePart1 = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02, maxRead));
        CommandAPDU readEF_PDFilePart2 = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02 + maxRead, dataPD.length-maxRead-0x02));

        CommandAPDU readEF_VDFile = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x08, dataVD.length));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));

            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(dataPD.length >> 8 & 0xFF, dataPD.length & 0xFF, 0x90, 0x00)));

            when(channel.transmit(readEF_PDFilePart1)).thenReturn(new ResponseAPDU(combineArrays(Arrays.copyOfRange(dataPD, 0, maxRead), ByteUtil.intToByteArray(0x90, 0x00))));
            when(channel.transmit(readEF_PDFilePart2)).thenReturn(new ResponseAPDU(combineArrays(Arrays.copyOfRange(dataPD, maxRead, dataPD.length), ByteUtil.intToByteArray(0x90, 0x00))));

            when(channel.transmit(readLengthEF_VD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x08, 0x00, 0x08+dataVD.length, 0x00, 0x00, 0x00, 0x00, 0x90, 0x00)));

            when(channel.transmit(readEF_VDFile)).thenReturn(new ResponseAPDU(combineArrays(dataVD, ByteUtil.intToByteArray(0x90, 0x00))));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            HealthCardData cardData = reader.read();
            assertNotNull(cardData);

            assertEquals("A123456789", cardData.getVersichertenID());
            assertEquals(LocalDate.of(2021, 10, 20), cardData.getGeburtsdatum());
            assertEquals("MyVorname", cardData.getVorname());
            assertEquals("MyNachname", cardData.getNachname());
            assertEquals("D", cardData.getGeschlecht());
            assertEquals("12345", cardData.getPlz());

        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testEmptyPDXMLVDFailLength() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));
        CommandAPDU readLengthEF_VD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x82, 0x00, 0x08));

        byte[] data = compressGZIP("<root></root>");

        CommandAPDU readEF_PDFile = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02, data.length));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, data.length+2, 0x90, 0x00)));
            when(channel.transmit(readEF_PDFile)).thenReturn(new ResponseAPDU(combineArrays(data, ByteUtil.intToByteArray(0x90, 0x00))));
            when(channel.transmit(readLengthEF_VD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            HealthCardData cardData = reader.read();
            assertNotNull(cardData);

            assertNull(cardData.getVersichertenID());
            assertNull(cardData.getVorname());
            assertNull(cardData.getNachname());
            assertNull(cardData.getGeburtsdatum());
            assertNull(cardData.getGeschlecht());
        } catch (CardException e) {
            fail(e);
        }
    }

    @Test
    public void testNullPDFile() {
        Card card = mock(Card.class);

        CardChannel channel = mock(CardChannel.class);

        CommandAPDU selectMF = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0X0C, 0x07, 0xD2, 0X76, 0x00, 0x01, 0x44, 0x80, 0x00));
        CommandAPDU selectHCA = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xA4, 0x04, 0x0C, 0x06, 0xD2, 0x76, 0x00, 0x00, 0x01, 0x02));
        CommandAPDU readLengthEF_PD = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x81, 0x00, 0x02));

        byte[] data = compressGZIP("<root></root>");

        CommandAPDU readEF_PDFile = new CommandAPDU(ByteUtil.intToByteArray(0x00, 0xB0, 0x00, 0x02, data.length));

        try {
            when(channel.transmit(selectMF)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(selectHCA)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x90, 0x00)));
            when(channel.transmit(readLengthEF_PD)).thenReturn(new ResponseAPDU(ByteUtil.intToByteArray(0x00, data.length+2, 0x90, 0x00)));
            when(channel.transmit(readEF_PDFile)).thenReturn(new ResponseAPDU(combineArrays(data, ByteUtil.intToByteArray(0x00, 0x00))));

            when(card.getBasicChannel()).thenReturn(channel);

            CardReader reader = new CardReader(card);

            HealthCardData cardData = reader.read();
            assertNull(cardData);
        } catch (CardException e) {
            fail(e);
        }
    }

    public byte[] compressGZIP(String input) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(output);
            gzip.write(input.getBytes("ISO-8859-15"));
            gzip.close();

            return output.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public byte[] combineArrays(byte[] arg1, byte[] arg2) {
        byte[] result = new byte[arg1.length + arg2.length];

        System.arraycopy(arg1, 0, result, 0, arg1.length);

        System.arraycopy(arg2, 0, result, arg1.length, arg2.length);

        return result;
    }

}