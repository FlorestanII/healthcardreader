package de.johannespollitt.healthcardreader.test;

import de.johannespollitt.healthcardreader.HealthCardData;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class HealthCardDataTest {

    @Test
    public void testSetterAndGetter() {

        HealthCardData data = new HealthCardData();

        assertTrue(data.setVersichertenID("A111111111"));
        assertFalse(data.setVersichertenID("A11111"));
        assertFalse(data.setVersichertenID(null));

        data.setVorname("Vorname");
        data.setNachname("Nachname");
        data.setGeburtsdatum(LocalDate.now());
        data.setGeschlecht("M");
        data.setVorsatzwort("Vorsatzwort");
        data.setNamenszusatz("Namenszusatz");
        data.setTitel("Titel");

        data.setPostfach_landCode("D");
        data.setPostfach_plz("12345");
        data.setPostfach_postfach("12 34 56");
        data.setPostfach_ort("Ort");

        data.setLandCode("D");
        data.setPlz("67890");
        data.setOrt("Ort");
        data.setStrasse("Hauptstrasse");
        data.setHausnummer("1");
        data.setAnschriftzusatz("Zusatz");

        data.setVersicherungsschutz_begin(LocalDate.now());
        data.setVersicherungsschutz_ende(LocalDate.now());
        data.setVersicherungsschutz_kostentraeger_kennung(123456);
        data.setVersicherungsschutz_kostentraeger_landCode("D");
        data.setVersicherungsschutz_kostentraeger_name("Kostentraeger");

        assertEquals("A111111111", data.getVersichertenID());
        assertEquals("Vorname", data.getVorname());
        assertEquals("Nachname", data.getNachname());
        assertEquals(LocalDate.now(), data.getGeburtsdatum());
        assertEquals("M", data.getGeschlecht());
        assertEquals("Vorsatzwort", data.getVorsatzwort());
        assertEquals("Namenszusatz", data.getNamenszusatz());
        assertEquals("Titel", data.getTitel());

        assertEquals("D", data.getPostfach_landCode());
        assertEquals("Ort", data.getPostfach_ort());
        assertEquals("12345", data.getPostfach_plz());
        assertEquals("12 34 56", data.getPostfach_postfach());

        assertEquals("67890", data.getPlz());
        assertEquals("Ort", data.getOrt());
        assertEquals("D", data.getLandCode());
        assertEquals("Hauptstrasse", data.getStrasse());
        assertEquals("1", data.getHausnummer());
        assertEquals("Zusatz", data.getAnschriftzusatz());

        assertEquals(LocalDate.now(), data.getVersicherungsschutz_begin());
        assertEquals(LocalDate.now(), data.getVersicherungsschutz_ende());
        assertEquals(123456, data.getVersicherungsschutz_kostentraeger_kennung());
        assertEquals("D", data.getVersicherungsschutz_kostentraeger_landCode());
        assertEquals("Kostentraeger", data.getVersicherungsschutz_kostentraeger_name());

    }

}