package de.johannespollitt.healthcardreader;

import de.johannespollitt.healthcardreader.command.CardReader;

import javax.smartcardio.*;

public class TerminalListener extends Thread {

    public static final long UPDATE_INTERVAL = 10000;

    private final HealthCardReader reader;
    private final CardTerminal terminal;

    private volatile boolean running = false;

    private volatile boolean cardPresent = false;

    private volatile HealthCardData currentCard = null;

    public TerminalListener(HealthCardReader reader, CardTerminal terminal) {
        if (reader == null) {
            throw new IllegalArgumentException("The HealthCardReader must not be null!");
        }

        if (terminal == null) {
            throw new IllegalArgumentException("The CardTerminal must not be null!");
        }

        this.reader = reader;
        this.terminal = terminal;

        setName(terminal.getName());
    }

    @Override
    public void run() {
        this.running = true;

        while (this.running && this.reader.isRunning()) {
            try {
                if (this.terminal.isCardPresent()) {
                    if (!this.cardPresent) {
                        cardInserted();
                        this.cardPresent = true;
                    }

                    this.terminal.waitForCardAbsent(UPDATE_INTERVAL);
                } else {
                    if (this.cardPresent) {
                        this.cardPresent = false;
                        cardRemoved();
                    }

                    this.terminal.waitForCardPresent(UPDATE_INTERVAL);
                }
            } catch (CardException e) {
                stopListener();
                HealthCardReader.LOGGER.error("Error during card operation: " + e.getMessage(), e);
            } catch (Exception e) {
                HealthCardReader.LOGGER.error("Error during update of terminal : " + e.getMessage(), e);
            }
        }
        
        this.running = false;
    }

    private void cardInserted() throws CardException {
        if (readCard()) {

            if (this.reader.getCardInsertCallback() != null) {
                try {
                    this.reader.getCardInsertCallback().accept(this.terminal.getName(), this.currentCard);
                } catch (Exception e) {
                    HealthCardReader.LOGGER.error("Error during card insert callback: " + e.getMessage(), e);
                }
            }

        } else {
            if (this.reader.getCardReadFailedCallback() != null) {
                try {
                    this.reader.getCardReadFailedCallback().accept(this.terminal.getName());
                } catch (Exception e) {
                    HealthCardReader.LOGGER.error("Error during card failed read callback: " + e.getMessage(), e);
                }
            }
        }
    }

    private void cardRemoved() {
        if (this.reader.getCardRemoveCallback() != null) {
            try {
                this.reader.getCardRemoveCallback().accept(this.terminal.getName(), this.currentCard);
            } catch (Exception e) {
                HealthCardReader.LOGGER.error("Error during card removed callback: " + e.getMessage(), e);
            }
        }

        this.currentCard = null;
    }

    private boolean readCard() throws CardException {
        Card card = this.terminal.connect("*");

        CardReader reader = new CardReader(card);

        this.currentCard = reader.read();

        card.disconnect(false);

        return this.currentCard != null;
    }

    public boolean isRunning() {
        return this.running;
    }

    public void stopListener() {
        this.running = false;
    }

    public boolean isCardPresent() {
        return this.cardPresent;
    }

}
