package de.johannespollitt.healthcardreader;

import java.time.LocalDate;

public class HealthCardData {

    private String versicherten_id;

    //Person
    private LocalDate geburtsdatum;
    private String vorname;
    private String nachname;
    private String geschlecht;
    private String vorsatzwort;
    private String namenszusatz;
    private String titel;

    //Postfachadresse
    private String postfach_plz;
    private String postfach_ort;
    private String postfach_postfach;
    private String postfach_landCode;

    //Strassenadresse
    private String plz;
    private String ort;
    private String landCode;
    private String strasse;
    private String hausnummer;
    private String anschriftzusatz;

    //Allgemeine Versicherungsdaten
    private LocalDate versicherungsschutz_begin;
    private LocalDate versicherungsschutz_ende;
    private int versicherungsschutz_kostentraeger_kennung;
    private String versicherungsschutz_kostentraeger_landCode;
    private String versicherungsschutz_kostentraeger_name;

    public HealthCardData() {

    }

    public String getVersichertenID() {
        return this.versicherten_id;
    }

    public boolean setVersichertenID(String versichertenID) {
        if (versichertenID != null && versichertenID.matches("[A-Z][0-9]{8}[0-9]")) {
            this.versicherten_id = versichertenID;
            return true;
        }

        return false;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getVorsatzwort() {
        return vorsatzwort;
    }

    public void setVorsatzwort(String vorsatzwort) {
        this.vorsatzwort = vorsatzwort;
    }

    public String getNamenszusatz() {
        return namenszusatz;
    }

    public void setNamenszusatz(String namenszusatz) {
        this.namenszusatz = namenszusatz;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getPostfach_plz() {
        return postfach_plz;
    }

    public void setPostfach_plz(String postfach_plz) {
        this.postfach_plz = postfach_plz;
    }

    public String getPostfach_ort() {
        return postfach_ort;
    }

    public void setPostfach_ort(String postfach_ort) {
        this.postfach_ort = postfach_ort;
    }

    public String getPostfach_postfach() {
        return postfach_postfach;
    }

    public void setPostfach_postfach(String postfach_postfach) {
        this.postfach_postfach = postfach_postfach;
    }

    public String getPostfach_landCode() {
        return postfach_landCode;
    }

    public void setPostfach_landCode(String postfach_landCode) {
        this.postfach_landCode = postfach_landCode;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getLandCode() {
        return landCode;
    }

    public void setLandCode(String landCode) {
        this.landCode = landCode;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getAnschriftzusatz() {
        return anschriftzusatz;
    }

    public void setAnschriftzusatz(String anschriftzusatz) {
        this.anschriftzusatz = anschriftzusatz;
    }

    public LocalDate getVersicherungsschutz_begin() {
        return versicherungsschutz_begin;
    }

    public void setVersicherungsschutz_begin(LocalDate versicherungsschutz_begin) {
        this.versicherungsschutz_begin = versicherungsschutz_begin;
    }

    public LocalDate getVersicherungsschutz_ende() {
        return versicherungsschutz_ende;
    }

    public void setVersicherungsschutz_ende(LocalDate versicherungsschutz_ende) {
        this.versicherungsschutz_ende = versicherungsschutz_ende;
    }

    public int getVersicherungsschutz_kostentraeger_kennung() {
        return versicherungsschutz_kostentraeger_kennung;
    }

    public void setVersicherungsschutz_kostentraeger_kennung(int versicherungsschutz_kostentraeger_kennung) {
        this.versicherungsschutz_kostentraeger_kennung = versicherungsschutz_kostentraeger_kennung;
    }

    public String getVersicherungsschutz_kostentraeger_landCode() {
        return versicherungsschutz_kostentraeger_landCode;
    }

    public void setVersicherungsschutz_kostentraeger_landCode(String versicherungsschutz_kostentraeger_landCode) {
        this.versicherungsschutz_kostentraeger_landCode = versicherungsschutz_kostentraeger_landCode;
    }

    public String getVersicherungsschutz_kostentraeger_name() {
        return versicherungsschutz_kostentraeger_name;
    }

    public void setVersicherungsschutz_kostentraeger_name(String versicherungsschutz_kostentraeger_name) {
        this.versicherungsschutz_kostentraeger_name = versicherungsschutz_kostentraeger_name;
    }

}
