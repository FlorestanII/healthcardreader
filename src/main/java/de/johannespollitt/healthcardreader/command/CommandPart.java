package de.johannespollitt.healthcardreader.command;

public class CommandPart {

    public static final CommandPart APPLICATION_ID_MF = new CommandPart(0xD2, 0x76, 0x00, 0x01, 0x44, 0x80, 0x00);
    public static final CommandPart APPLICATION_ID_HCA = new CommandPart(0xD2, 0x76, 0x00, 0x00, 0x01, 0x02);

    public static final CommandPart COMMAND_SELECT_APP_NO_RESPONSE = new CommandPart(0x00, 0xA4, 0x04, 0x0C);
    public static final CommandPart COMMAND_READ_BINARY = new CommandPart(0x00, 0xB0);

    public static final CommandPart FILE_SHORT_ID_EF_PD = new CommandPart(0x81);
    public static final CommandPart FILE_SHORT_ID_EF_VD = new CommandPart(0x82);

    private final byte[] identifier;

    public CommandPart(int... identifier) {
        this.identifier = ByteUtil.intToByteArray(identifier);
    }

    public byte[] getIdentifier() {
        return this.identifier;
    }

}
