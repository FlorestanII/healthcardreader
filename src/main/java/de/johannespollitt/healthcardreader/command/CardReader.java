package de.johannespollitt.healthcardreader.command;

import de.johannespollitt.healthcardreader.HealthCardData;
import de.johannespollitt.healthcardreader.HealthCardReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.smartcardio.*;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class CardReader {

    private static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    static {
        try {
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        } catch (ParserConfigurationException e) {
            HealthCardReader.LOGGER.error("Setup of XML Factory failed: " + e.getMessage(), e);
        }
    }

    private final CardChannel channel;

    public CardReader(Card card) {
        if (card == null) {
            throw new IllegalArgumentException("The card must not be null!");
        }

        this.channel = card.getBasicChannel();
    }

    public HealthCardData read() {

        try {
            if (!selectMF()) {
                return null;
            }

            if (!selectHCA()) {
                return null;
            }

            int pdLength = readEF_PD_length();

            if (pdLength <= 0) {
                return null;
            }

            String pdData = readFile(0x02, pdLength);

            if (pdData == null) {
                //Reading of PD file failed
                return null;
            }

            String vdData = null;

            int[] vdStartEnd = readEF_VD_length();

            if (vdStartEnd != null && (vdStartEnd[1]-vdStartEnd[0]) > 0) {
                vdData = readFile(vdStartEnd[0], vdStartEnd[1]-vdStartEnd[0]);
            }

            return readFromXML_5_2_0(pdData, vdData);
        } catch (Exception e) {
            e.printStackTrace();
            HealthCardReader.LOGGER.error("Error during reading a new card: " + e.getMessage(), e);
        }

        return null;
    }

    private boolean selectMF() throws CardException {
        ResponseAPDU response = this.channel.transmit(new CommandAPDU(HealthCardCommandGenerator.SELECT_MF));

        return response.getSW() == 0x9000;
    }

    private boolean selectHCA() throws CardException {
        ResponseAPDU response = this.channel.transmit(new CommandAPDU(HealthCardCommandGenerator.SELECT_HCA));

        return response.getSW() == 0x9000;
    }

    private int readEF_PD_length() throws CardException {
        ResponseAPDU response = this.channel.transmit(new CommandAPDU(HealthCardCommandGenerator.SELECT_EF_PD));

        if (response.getSW() != 0x9000) {
            return -1;
        }

        return (response.getData()[0] << 8) + response.getData()[1] - 2;
    }

    private int[] readEF_VD_length() throws CardException {
        ResponseAPDU response = this.channel.transmit(new CommandAPDU(HealthCardCommandGenerator.SELECT_EF_VD));

        if (response.getSW() != 0x9000) {
            return null;
        }

        int startVD = (response.getData()[0] << 8) + response.getData()[1];
        int endVD = (response.getData()[2] << 8) + response.getData()[3];

        return new int[]{startVD, endVD};
    }

    private String readFile(int offset, int length) throws CardException, IOException {
        ByteArrayOutputStream readData = new ByteArrayOutputStream();
        int bytes_read = 0;

        int max_read = 0xFC; //TODO read out of EF.ATR

        int pointer = offset;

        while (bytes_read < length) {
            int bytes_left = length - bytes_read;

            int readLen = Math.min(bytes_left, max_read);

            ResponseAPDU response = this.channel.transmit(new CommandAPDU(HealthCardCommandGenerator.generateReadFileCommand(pointer, readLen)));

            if (response.getSW() != 0x9000) {
                return null;
            }

            pointer += readLen;
            bytes_read += response.getData().length;

            readData.write(response.getData());

        }

        readData.write(new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

        return ByteUtil.ungzip(readData.toByteArray());
    }

    public HealthCardData readFromXML_5_2_0(String pdXML, String vdXML) throws ParserConfigurationException, IOException, SAXException {

        if (pdXML == null) {
            throw new IllegalStateException("The pdXML must not be null!");
        }

        HealthCardData cardData = new HealthCardData();

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document docPD = db.parse(new InputSource(new StringReader(pdXML)));
        docPD.getDocumentElement().normalize();
        getTextContentOfChild(docPD.getDocumentElement(), "Versicherten_ID").ifPresent(cardData::setVersichertenID);

        getChildElement(docPD.getDocumentElement(), "Person").ifPresent(p -> readPerson_5_2_0(p, cardData));

        if (vdXML != null) {
            Document docVD = db.parse(new InputSource(new StringReader(vdXML)));
            docVD.getDocumentElement().normalize();

            readVersicherungsdaten_5_2_0(docVD.getDocumentElement(), cardData);
        }

        return cardData;
    }

    private void readPerson_5_2_0(Element person, HealthCardData readTo) {

        //Person
        getTextContentOfChild(person, "Geburtsdatum")
                .ifPresent(gb -> readTo.setGeburtsdatum(LocalDate.parse(gb, DateTimeFormatter.BASIC_ISO_DATE)));
        getTextContentOfChild(person, "Vorname").ifPresent(readTo::setVorname);
        getTextContentOfChild(person, "Nachname").ifPresent(readTo::setNachname);
        getTextContentOfChild(person, "Geschlecht").ifPresent(readTo::setGeschlecht);
        getTextContentOfChild(person, "Vorsatzwort").ifPresent(readTo::setVorsatzwort);
        getTextContentOfChild(person, "Namenszusatz").ifPresent(readTo::setNamenszusatz);
        getTextContentOfChild(person, "Titel").ifPresent(readTo::setTitel);

        getChildElement(person, "PostfachAdresse")
                .ifPresent(postfachAdresse -> readPostfachAdresse(postfachAdresse, readTo));

        getChildElement(person, "StrassenAdresse")
                .ifPresent(strassenAdresse -> readStrassenAdresse(strassenAdresse, readTo));

    }

    private void readPostfachAdresse(Element postfachAdresse, HealthCardData readTo) {
        getTextContentOfChild(postfachAdresse, "Postleitzahl").ifPresent(readTo::setPostfach_plz);
        getTextContentOfChild(postfachAdresse, "Ort").ifPresent(readTo::setPostfach_ort);
        getTextContentOfChild(postfachAdresse, "Postfach").ifPresent(readTo::setPostfach_postfach);
        getTextContentOfChild(postfachAdresse, "Wohnsitzlaendercode").ifPresent(readTo::setPostfach_landCode);
    }

    private void readStrassenAdresse(Element strassenAdresse, HealthCardData readTo) {
        getTextContentOfChild(strassenAdresse, "Postleitzahl").ifPresent(readTo::setPlz);
        getTextContentOfChild(strassenAdresse, "Ort").ifPresent(readTo::setOrt);
        getTextContentOfChild(strassenAdresse, "Strasse").ifPresent(readTo::setStrasse);
        getTextContentOfChild(strassenAdresse, "Hausnummer").ifPresent(readTo::setHausnummer);
        getTextContentOfChild(strassenAdresse, "Anschriftenzusatz").ifPresent(readTo::setAnschriftzusatz);
        getTextContentOfChild(strassenAdresse, "Wohnsitzlaendercode").ifPresent(readTo::setLandCode);
    }

    private void readVersicherungsdaten_5_2_0(Element vd, HealthCardData readTo) {
        getTextContentOfChild(vd, "Beginn")
                .ifPresent(beginn -> readTo.setVersicherungsschutz_begin(LocalDate.parse(beginn, DateTimeFormatter.BASIC_ISO_DATE)));
        getTextContentOfChild(vd, "EndeVersicherungsnachweis")
                .ifPresent(end -> readTo.setVersicherungsschutz_ende(LocalDate.parse(end, DateTimeFormatter.BASIC_ISO_DATE)));

        getTextContentOfChild(vd, "Kostentraegerkennung").ifPresent(kennung -> readTo.setVersicherungsschutz_kostentraeger_kennung(Integer.parseInt(kennung)));
        getTextContentOfChild(vd, "Kostentraegerlaendercode").ifPresent(readTo::setVersicherungsschutz_kostentraeger_landCode);
        getTextContentOfChild(vd, "Name").ifPresent(readTo::setVersicherungsschutz_kostentraeger_name);

    }

    private Optional<Element> getChildElement(Element parent, String childName) {
        NodeList nodes = parent.getElementsByTagName(childName);

        if (nodes.getLength() > 0) {
            if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
                return Optional.of((Element) nodes.item(0));
            }
        }

        return Optional.empty();
    }

    private Optional<String> getTextContentOfChild(Element parent, String childName) {
        NodeList nodes = parent.getElementsByTagName(childName);

        if (nodes.getLength() > 0) {
            return Optional.of(nodes.item(0).getTextContent());
        }

        return Optional.empty();
    }

}
