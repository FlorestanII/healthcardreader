package de.johannespollitt.healthcardreader.command;

import java.nio.ByteBuffer;

public class HealthCardCommandGenerator {

    public static final byte[] SELECT_MF = generateApplicationSelectCommand(CommandPart.APPLICATION_ID_MF);
    public static final byte[] SELECT_HCA = generateApplicationSelectCommand(CommandPart.APPLICATION_ID_HCA);

    public static final byte[] SELECT_EF_PD = generateSelectFileCommand(CommandPart.FILE_SHORT_ID_EF_PD, (byte) 0x00, 0x02);
    public static final byte[] SELECT_EF_VD = generateSelectFileCommand(CommandPart.FILE_SHORT_ID_EF_VD, (byte) 0x00, 0x08);

    public static byte[] generateApplicationSelectCommand(CommandPart application) {
        CommandPart command = CommandPart.COMMAND_SELECT_APP_NO_RESPONSE;

        return combineToCommand(command, new CommandPart(application.getIdentifier().length), application);
    }

    public static byte[] generateSelectFileCommand(CommandPart file, byte offset, int length) {
        CommandPart command = CommandPart.COMMAND_READ_BINARY;
        return combineToCommand(command, file, new CommandPart(offset, length));
    }

    public static byte[] generateReadFileCommand(int offset, int length) {
        CommandPart command = CommandPart.COMMAND_READ_BINARY;
        return combineToCommand(command, new CommandPart(offset >> 8 & 0xFF, offset & 0xFF, length));
    }

    public static byte[] combineToCommand(CommandPart... parts) {
        ByteBuffer buffer = ByteBuffer.allocate(256);

        for (CommandPart part : parts) {
            buffer.put(part.getIdentifier());
        }

        buffer.flip();

        byte[] command = new byte[buffer.limit()];
        buffer.get(command);

        return command;
    }

}
