package de.johannespollitt.healthcardreader.command;

import de.johannespollitt.healthcardreader.HealthCardReader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class ByteUtil {

    public static byte[] intToByteArray(int... input) {
        byte[] result = new byte[input.length];

        for (int i = 0; i < input.length; i++) {
            result[i] = (byte) input[i];
        }

        return result;
    }

    public static String ungzip(byte[] input) {
        try {
            ByteArrayInputStream bytein = new ByteArrayInputStream(input);
            GZIPInputStream gzin = new GZIPInputStream(bytein);
            ByteArrayOutputStream byteout = new ByteArrayOutputStream();

            int res = 0;
            byte[] buf = new byte[1024];

            while(res >= 0) {
                res = gzin.read(buf, 0, buf.length);

                if (res > 0) {
                    byteout.write(buf, 0, res);
                }
            }

            return byteout.toString("ISO-8859-15");
        } catch (IOException e) {
            HealthCardReader.LOGGER.error("Error during unzipping read data: " + e.getMessage(), e);
        }

        return null;
    }

}
