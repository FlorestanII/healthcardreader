package de.johannespollitt.healthcardreader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactory;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class HealthCardReader {

    public static final Logger LOGGER = LoggerFactory.getLogger(HealthCardReader.class);

    public static final long TERMINAL_LIST_UPDATE_INTERVAL = 10000;

    private final TerminalFactory factory;

    private volatile boolean running = false;

    private Thread terminalUpdater;

    private final Map<String, TerminalListener> listeners;

    private volatile Consumer<String> newTerminalCallback;
    private volatile Consumer<String> removeTerminalCallback;
    private volatile BiConsumer<String, HealthCardData> cardInsertCallback;
    private volatile BiConsumer<String, HealthCardData> cardRemoveCallback;
    private volatile Consumer<String> cardReadFailedCallback;

    public HealthCardReader() {
        this.factory = TerminalFactory.getDefault();
        this.listeners = new HashMap<>();
    }

    public void init() {
        if (this.running) {
            throw new IllegalStateException("This Health Card Reader is already initialised and running!");
        }

        this.running = true;

        this.terminalUpdater = new Thread(this::updateTerminals);
        this.terminalUpdater.setName("Terminal Updater");
        this.terminalUpdater.start();

    }

    public void cleanup() {
        this.running = false;

        for (TerminalListener listener : this.listeners.values()) {
            listener.stopListener();
        }

        this.listeners.clear();

    }

    private void updateTerminals() {
        while (this.running) {
            try {
                CardTerminals terminals = this.factory.terminals();

                List<String> toRemove = new ArrayList<>();

                for (String listener : this.listeners.keySet()) {
                    if (terminals.getTerminal(listener) == null || !this.listeners.get(listener).isRunning()) {
                        this.listeners.get(listener).stopListener();
                        toRemove.add(listener);
                    }
                }

                for (String listenerToRemove : toRemove) {
                    this.listeners.remove(listenerToRemove);

                    if (this.removeTerminalCallback != null) {
                        this.removeTerminalCallback.accept(listenerToRemove);
                    }
                }

                for (CardTerminal terminal : terminals.list()) {
                    if (!this.listeners.containsKey(terminal.getName())) {
                        //new terminal detected -> create and start new listener
                        TerminalListener listener = new TerminalListener(this, terminal);
                        listener.start();
                        this.listeners.put(terminal.getName(), listener);

                        if (this.newTerminalCallback != null) {
                            this.newTerminalCallback.accept(terminal.getName());
                        }
                    }
                }

                Thread.sleep(TERMINAL_LIST_UPDATE_INTERVAL);

            } catch (Exception e) {
                LOGGER.error("Error while updating terminals: " + e.getMessage(), e);
            }
        }
    }

    /**
     * Returns a collection with all currently available SmartCard terminals.
     * <br /><br />
     * If no SmartCard terminal is connected, an empty collection will be returned.
     * <br /><br />
     * WARNING: It can take up to {@link #TERMINAL_LIST_UPDATE_INTERVAL} milliseconds,
     * until the returned collection is up to date.
     *
     * @return all currently available SmartCard terminals
     */
    public Collection<String> getAvailableTerminals() {
        return this.listeners.keySet();
    }

    public boolean areTerminalsAvailable() {
        return getAvailableTerminals().size() > 0;
    }

    public void setNewTerminalCallback(Consumer<String> newTerminalCallback) {
        this.newTerminalCallback = newTerminalCallback;
    }

    public void setTerminalRemoveCallback(Consumer<String> terminalRemoveCallback) {
        this.removeTerminalCallback = terminalRemoveCallback;
    }

    //async
    public void setCardInsertCallback(BiConsumer<String, HealthCardData> cardInsertCallback) {
        this.cardInsertCallback = cardInsertCallback;
    }

    public BiConsumer<String, HealthCardData> getCardInsertCallback() {
        return this.cardInsertCallback;
    }

    //async
    public void setCardRemoveCallback(BiConsumer<String, HealthCardData> cardRemoveCallback) {
        this.cardRemoveCallback = cardRemoveCallback;
    }

    public BiConsumer<String, HealthCardData> getCardRemoveCallback() {
        return this.cardRemoveCallback;
    }

    //async
    public void setCardReadFailedCallback(Consumer<String> cardReadFailed) {
        this.cardReadFailedCallback = cardReadFailed;
    }

    public Consumer<String> getCardReadFailedCallback() {
        return this.cardReadFailedCallback;
    }

    public boolean isRunning() {
        return this.running;
    }

}
